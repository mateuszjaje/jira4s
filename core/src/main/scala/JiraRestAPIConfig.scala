package io.gitlab.mateuszjaje.jiraclient

case class JiraRestAPIConfig(
    debug: Boolean = false,
)
